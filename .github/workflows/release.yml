name: 'release'

on:
  push:
    tags: [ '*' ]
    paths-ignore:
    - '!.github/release.yml'
    - 'docs/**'
    - 'resource/**'
    - '.coveralls.yml'
    - '.readthedocs.yml'
    - '.git*'
    - '*.md'
    - 'LICENSE'

jobs:
  check:
    name: 'check by cppcheck'
    runs-on: ubuntu-latest
    steps:
    - name: 'install cppcheck'
      run: sudo apt-get install cppcheck
    - name: 'checkout'
      uses: actions/checkout@v3
    - name: 'check'
      run: cppcheck ./ --enable=warning,performance,portability --xml-version=2 --error-exitcode=1 -i external/ -i build/ -i tools/

  windows:
    name: 'for windows'
    runs-on: windows-latest
    needs: check
    steps:
    - name: 'checkout the project'
      uses: actions/checkout@v3
    - name: 'update submodules'
      run: |
        git submodule update --init external/rapidjson
        git submodule update --init external/draco
    - name: 'add msbuild'
      uses: microsoft/setup-msbuild@v1.0.2
    - name: 'build and make'
      run: |
        cmake -B build/win64 -G "Visual Studio 17 2022" -A "x64" -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/win64/ --target libgltf --config Debug
        cmake --build build/win64/ --target libgltf --config Release
        cmake -B build/win32 -G "Visual Studio 17 2022" -A "Win32" -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/win32/ --target libgltf --config Debug
        cmake --build build/win32/ --target libgltf --config Release
    - name: 'ready artifact'
      run: |
        cmake --install build/win64/source/ --prefix output/win64/
        xcopy /D /S /Y external\draco\src\draco\*.h output\win64\include\draco\
        xcopy /D /S /Y build\win64\draco\*.h output\win64\include\draco\
        xcopy /D /S /Y build\win64\external\draco\Release\draco.lib output\win64\lib\Release\
        xcopy /D /S /Y build\win64\external\draco\Debug\dracod.lib output\win64\lib\Debug\
        cmake --install build/win32/source/ --prefix output/win32/
        xcopy /D /S /Y external\draco\src\draco\*.h output\win32\include\draco\
        xcopy /D /S /Y build\win64\draco\*.h output\win32\include\draco\
        xcopy /D /S /Y build\win32\external\draco\Release\draco.lib output\win32\lib\Release\
        xcopy /D /S /Y build\win32\external\draco\Debug\dracod.lib output\win32\lib\Debug\
    - name: 'upload artifact'
      uses: actions/upload-artifact@v1.0.0
      with:
        name: libgltf.windows
        path: output/

  linux:
    name: 'for linux'
    runs-on: ubuntu-latest
    needs: check
    steps:
    - name: 'checkout the project'
      uses: actions/checkout@v3
    - name: 'update submodules'
      run: |
        git submodule update --init external/rapidjson
        git submodule update --init external/draco
    - name: 'build and make'
      run: |
        cmake -B build -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build --target libgltf --config Release
    - name: 'ready artifact'
      run: |
        cmake --install build/source/ --prefix output/linux/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/linux/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/draco/ output/linux/include/draco/
        cp -a build/external/draco/libdraco.a output/linux/lib/
    - name: 'upload artifact'
      uses: actions/upload-artifact@v1.0.0
      with:
        name: libgltf.linux
        path: output/

  macos:
    name: 'for macos'
    runs-on: macos-latest
    needs: check
    steps:
    - name: 'checkout the project'
      uses: actions/checkout@v3
    - name: 'update submodules'
      run: |
        git submodule update --init external/rapidjson
        git submodule update --init external/draco
    - name: 'build and make'
      run: |
        cmake -B build -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build --target libgltf --config Release
    - name: 'ready artifact'
      run: |
        cmake --install build/source/ --prefix output/macos/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/macos/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/draco/ output/macos/include/draco/
        cp build/external/draco/libdraco.a output/macos/lib/
    - name: 'upload artifact'
      uses: actions/upload-artifact@v1.0.0
      with:
        name: libgltf.macos
        path: output/

  android:
    name: 'for android api19'
    runs-on: ubuntu-latest
    needs: check
    steps:
    - name: 'install'
      run: |
        sudo apt-get install ninja-build
    - name: 'checkout the project'
      uses: actions/checkout@v3
    - name: 'update submodules'
      run: |
        git submodule update --init external/rapidjson
        git submodule update --init external/draco
    - name: 'build and make'
      run: |
        cmake -B build/armeabi-v7a -G "Ninja" -DCMAKE_MAKE_PROGRAM=/usr/bin/ninja -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK_HOME/build/cmake/android.toolchain.cmake -DANDROID_ABI=armeabi-v7a -DANDROID_NDK=$ANDROID_NDK_HOME -DANDROID_NATIVE_API_LEVEL=19 -DANDROID_TOOLCHAIN=clang -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/armeabi-v7a --target libgltf --config Release
        cmake -B build/arm64-v8a -G "Ninja" -DCMAKE_MAKE_PROGRAM=/usr/bin/ninja -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK_HOME/build/cmake/android.toolchain.cmake -DANDROID_ABI=arm64-v8a -DANDROID_NDK=$ANDROID_NDK_HOME -DANDROID_NATIVE_API_LEVEL=19 -DANDROID_TOOLCHAIN=clang -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/arm64-v8a --target libgltf --config Release
        cmake -B build/x86 -G "Ninja" -DCMAKE_MAKE_PROGRAM=/usr/bin/ninja -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK_HOME/build/cmake/android.toolchain.cmake -DANDROID_ABI=x86 -DANDROID_NDK=$ANDROID_NDK_HOME -DANDROID_NATIVE_API_LEVEL=19 -DANDROID_TOOLCHAIN=clang -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/x86 --target libgltf --config Release
        cmake -B build/x86_64 -G "Ninja" -DCMAKE_MAKE_PROGRAM=/usr/bin/ninja -DCMAKE_TOOLCHAIN_FILE=$ANDROID_NDK_HOME/build/cmake/android.toolchain.cmake -DANDROID_ABI=x86_64 -DANDROID_NDK=$ANDROID_NDK_HOME -DANDROID_NATIVE_API_LEVEL=19 -DANDROID_TOOLCHAIN=clang -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/x86_64 --target libgltf --config Release
    - name: 'ready artifact'
      run: |
        cmake --install build/armeabi-v7a/source/ --prefix output/armeabi-v7a/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/armeabi-v7a/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/armeabi-v7a/draco/ output/armeabi-v7a/include/draco/
        cp -a build/armeabi-v7a/external/draco/libdraco.a output/armeabi-v7a/lib/
        cmake --install build/arm64-v8a/source/ --prefix output/arm64-v8a/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/arm64-v8a/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/arm64-v8a/draco/ output/arm64-v8a/include/draco/
        cp -a build/arm64-v8a/external/draco/libdraco.a output/arm64-v8a/lib/
        cmake --install build/x86/source/ --prefix output/x86/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/x86/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/x86/draco/ output/x86/include/draco/
        cp -a build/x86/external/draco/libdraco.a output/x86/lib/
        cmake --install build/x86_64/source/ --prefix output/x86_64/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/x86_64/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/x86_64/draco/ output/x86_64/include/draco/
        cp -a build/x86_64/external/draco/libdraco.a output/x86_64/lib/
    - name: 'upload artifact'
      uses: actions/upload-artifact@v1.0.0
      with:
        name: libgltf.android.api19
        path: output/

  ios:
    name: 'for ios'
    runs-on: macos-latest
    needs: check
    steps:
    - name: 'checkout the project'
      uses: actions/checkout@v3
    - name: 'update submodules'
      run: |
        git submodule update --init external/rapidjson
        git submodule update --init external/draco
        git submodule update --init external/ios-cmake
    - name: 'build and make'
      run: |
        cmake -B build/iphoneos -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../../external/ios-cmake/toolchain/iOS.cmake -DIOS_PLATFORM=OS -DLIBGLTF_PLATFORM_IOS=TRUE -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/iphoneos --target libgltf --config Release
        cmake -B build/watchos -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../../external/ios-cmake/toolchain/iOS.cmake -DIOS_PLATFORM=WATCHOS -DLIBGLTF_PLATFORM_IOS=TRUE -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/watchos --target libgltf --config Release
        cmake -B build/simulator -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../../external/ios-cmake/toolchain/iOS.cmake -DIOS_PLATFORM=SIMULATOR -DLIBGLTF_PLATFORM_IOS=TRUE -DLIBGLTF_WITH_GOOGLE_DRACO=TRUE .
        cmake --build build/simulator --target libgltf --config Release
    - name: 'ready artifact'
      run: |
        cmake --install build/iphoneos/source/ --prefix output/iphoneos/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/iphoneos/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/iphoneos/draco/ output/iphoneos/include/draco/
        cp build/iphoneos/external/draco/libdraco.a output/iphoneos/lib/
        cmake --install build/watchos/source/ --prefix output/watchos/
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/watchos/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/iphoneos/draco/ output/watchos/include/draco/
        cp build/watchos/external/draco/libdraco.a output/watchos/lib/
        cmake --install build/simulator/source/ --prefix output/simulator
        rsync -a --include='*.h' -f 'hide,! */' external/draco/src/draco/ output/simulator/include/draco/
        rsync -a --include='*.h' -f 'hide,! */' build/iphoneos/draco/ output/simulator/include/draco/
        cp build/simulator/external/draco/libdraco.a output/simulator/lib/
    - name: 'upload artifact'
      uses: actions/upload-artifact@v1.0.0
      with:
        name: libgltf.ios
        path: output/

  release:
    name: 'release'
    runs-on: ubuntu-latest
    needs: [ windows, linux, macos, android, ios ]
    steps:
    - name: 'get version by tag'
      id: get_version
      uses: battila7/get-version-action@v2
    - name: 'create release'
      id: create_release
      uses: actions/create-release@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        tag_name: '${{ github.ref }}'
        release_name: 'Release v${{ github.ref }}'
        body: ''
        draft: false
        prerelease: false
    - name: 'download artifact'
      uses: actions/download-artifact@v2
      with:
        path: 'output/'
    - name: 'package release asset'
      run: |
        cd libgltf.windows/ && zip -r package.zip ./* && cd ../
        cd libgltf.linux/ && zip -r package.zip ./* && cd ../
        cd libgltf.macos/ && zip -r package.zip ./* && cd ../
        cd libgltf.android.api19/ && zip -r package.zip ./* && cd ../
        cd libgltf.ios/ && zip -r package.zip ./* && cd ../
      working-directory: 'output/'
    - name: 'upload release asset windows'
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ steps.create_release.outputs.upload_url }}
        asset_path: 'output/libgltf.windows/package.zip'
        asset_name: 'libgltf.v${{ steps.get_version.outputs.version }}.windows.zip'
        asset_content_type: application/zip
    - name: 'upload release asset linux'
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ steps.create_release.outputs.upload_url }}
        asset_path: 'output/libgltf.linux/package.zip'
        asset_name: 'libgltf.v${{ steps.get_version.outputs.version }}.linux.zip'
        asset_content_type: application/zip
    - name: 'upload release asset macos'
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ steps.create_release.outputs.upload_url }}
        asset_path: 'output/libgltf.macos/package.zip'
        asset_name: 'libgltf.v${{ steps.get_version.outputs.version }}.macos.zip'
        asset_content_type: application/zip
    - name: 'upload release asset android api19'
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ steps.create_release.outputs.upload_url }}
        asset_path: 'output/libgltf.android.api19/package.zip'
        asset_name: 'libgltf.v${{ steps.get_version.outputs.version }}.android.api19.zip'
        asset_content_type: application/zip
    - name: 'upload release asset ios'
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ steps.create_release.outputs.upload_url }}
        asset_path: 'output/libgltf.ios/package.zip'
        asset_name: 'libgltf.v${{ steps.get_version.outputs.version }}.ios.zip'
        asset_content_type: application/zip
